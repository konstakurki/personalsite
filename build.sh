mkdir output
cd ebook_code
#python3 shp.py ../etusivu/index.html latex>fi.tex
#pdflatex fi.tex
#pdflatex fi.tex
#pdflatex fi.tex
#rm fi.aux fi.log fi.out fi.toc
#mv fi.pdf ../output/kiipeilya-teoria-avaruudessa.pdf
#mv fi.tex ../output/kiipeilya-teoria-avaruudessa.tex

python3 shp.py ../etusivu/index.html html>../output/kta.html
python3 shp.py metadata-fi.xml metadata>../output/metadata.xml

pandoc -o ../output/kta.epub ../output/kta.html --toc --toc-depth=2 --epub-stylesheet='epub.css' --epub-metadata='../output/metadata.xml'

cd ../output


mv kta.html kiipeilya-teoria-avaruudessa.html
rm kiipeilya-teoria-avaruudessa.html
rm metadata.xml
mv kta.epub kiipeilya-teoria-avaruudessa.epub

ebook-convert kiipeilya-teoria-avaruudessa.epub kiipeilya-teoria-avaruudessa.mobi
