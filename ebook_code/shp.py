# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import subprocess
import datetime
import sys

def TOC(string):
    html = BeautifulSoup(open(string),'html5lib')
    toc = []
    for i in html.find_all('a',{'class':'toc'}):
        toc.append(i.get('href')+'index.html')
    return toc

def content(string):
    toc = TOC(string)
    cont = ''
    for i in toc:
        ch = BeautifulSoup(open(i),'html5lib').main
        cont = cont + str(ch)[6:-7]
    cont = BeautifulSoup(cont,'html5lib')
    return cont

def ref(d):
    doc = d
    links = doc.find_all('a','os')
    for link in links:
        href = link['href']
        if href[0:3] == '../':
            if href[-1] == '/':
                link['href'] = "#" + href[3:-1]
            else:
                link['href'] = "#" + href[3:]
    return doc

def lowerheaders(html):
    s = html
    for i in [5,4,3,2,1]:
        headers = s.find_all('h' + str(i))
        for j in headers:
            j.name = 'h' + str(i+1)
    return s

def singlepage(string):
    html = content(string)
    charset = html.new_tag('meta',charset='utf-8')
    html.head.append(charset)
    html = ref(html)
    #html = lowerheaders(html)
    return str(html)

def htmltoc(string):
    soup = BeautifulSoup(string,'html5lib')
    headers = soup.find_all(['h2','h3'])
    toc = ''
    for i in headers:
        item = '<a href="#%s">%s</a>' % (i['id'], i.contents[0])
        if i.name == 'h2':
            item = '</ul>\n</li>\n<li>%s\n<ul>\n' % item
        else:
            item = '<li>%s</li>\n' % item

        #print(item)
        toc = toc + item
        #if i.name == 'h2':
            #print('perkele')
        #else:
            #item = '<l
    toc = toc.replace('<ul>\n</ul>','')
    toc = '<ul>\n%s</ul></ul>' % toc[12:]
    string = string.replace('TableOfContentsGoesHere',toc)
    return string


def CommitId():
    idcmd = ['git','log','--pretty=format:"%H"','-n','1|cat']
    commitid = subprocess.check_output(idcmd)
    commitid = commitid.decode()
    return commitid[1:-1]

def Date():
    datecmd = ['git','log','--pretty=format:"%ad"','--date=short','-n','1|cat']
    date = subprocess.check_output(datecmd)
    date = date.decode()
    return date[1:-1]

def firstDate():
    datecmd = ['git','log','--reverse','--pretty=format:"%ad"','--date=short','-n','100000|cat']
    date = subprocess.check_output(datecmd)
    date = date.decode()
    return date[1:11]

def utf8tolatex(string):
    di = [['°','{\\degree}'],['−','{\\textminus}'],["–",'--']]
    for i in di:
        string = string.replace(i[0],i[1])
    return string

def citetoem(string):
    string = string.replace('<cite>','<em>')
    string = string.replace('</cite>','</em>')
    return string

def dateformat(string,lang):
    year = string[0:4]
    month = string[5:7]
    day = string[8:10]
    if lang == 'en':
        if month[0] == '0':
            month = month[1]
        month = ['January','February','March','April','May','June','July','August','September','October','November','December'][int(month)-1]
        if day[0] == '0':
            day = day[1]
        formatteddate = month + ' ' + day + ', ' + year
    elif lang == 'fi':
        if month[0] == '0':
            month = month[1]
        month = ['tammikuuta','helmikuuta','maaliskuuta','huhtikuuta','toukokuuta','kesäkuuta','heinäkuuta','elokuuta','syyskuuta','lokakuuta','marraskuuta','joulukuuta'][int(month)-1]
        if day[0] == '0':
            day = day[1]
        formatteddate = day + '. ' + month + ' ' + year
    else:
        formatteddate = string
    return formatteddate


def copytime():
    t1 = firstDate()[0:4]
    t2 = Date()[0:4]
    if t1 == t2:
        ct = str(t1)[0:4]
    else:
        ct = '('+t1+"–"+t2+')'
    return ct

def metadata(meta,document):
    for i in meta:
        document = document.replace(i + 'GoesHere',meta[i])
    document = document.replace('CommitIdGoesHere',CommitId())
    document = document.replace('CommitIdShortGoesHere',CommitId()[0:8])
    document = document.replace('DateGoesHere',dateformat(Date()))
    document = document.replace('CopyrightTimeGoesHere',copytime())
    return document


def latexheaders(string):
    string = string.replace('\\subsection{','\\chapter{')
    string = string.replace('\\subsubsection{','\\section{')
    string = string.replace('\\paragraph{','\\subsection{')
    return string

def link(string,n):
    n1 = string.find('\href{',n)
    if n1 != -1:
        n2 = string.find('}{',n1)
        n3 = n2
        n4 = n3 + 1
        while n4 != -1:
            n3 = string.find('}',n3+1)
            n4 = string.find('{',n4+1,n3)
        footnote = '\\footnote{\\url{' + string[n1+6:n2] + '}}'
        footnote = footnote.replace('\\&','&')
        footnote = footnote.replace('\\#','#')
        symbs = ['.',',',':',';']
        m = 0
        tr = 1
        while tr:
            tr = string[n3+1-m] in symbs
            if tr:
                m = m + 1
        string = string[0:n1] + string[n2+2:n3] + string[n3+1:n3+m+1] + footnote + string[n3+m+1:]
        output = [string,n3-m]
    else:
        output = [string,-1]
    return output

def links(string):
    o = [string,0]
    while o[1] != -1:
        o = link(o[0],o[1])
    return o[0]

def crossref(string,n):
    n1 = string.find('\\hyperref[',n)
    if n1 != -1:
        n2 = string.find(']{',n1)
        n3 = n2
        n4 = n3 + 1
        while n4 != -1:
            n3 = string.find('}',n3+1)
            n4 = string.find('{',n4+1,n3)
        string = string[0:n1] + string[n2+2:n3] + ' (ks. luku \\ref{' + string[n1+10:n2] + '}.)' + string[n3+1:]
        output = [string,n3]
    else:
        output = [string,-1]
    return output

def crossrefs(string):
    o = [string,0]
    while o[1] != -1:
        o = crossref(o[0],o[1])
    return o[0]


def emptyline1(string,n):
    n = string.find('</p>\n',n) + 5
    if string[n] != '\n' and n != 4:
        string = string[:n] + '\n<p>NoBlankLine348</p>\n\n' + string[n:]
    return [string,n]

def emptyline2(string,n):
    n = string.find('\n<p>',n) + 1
    if string[n - 2] != '\n' and n != 0:
        string = string[:n] + '\n<p>NoBlankLine348</p>\n\n' + string[n:]
    return [string,n]

def emptylines(stringg):
    string = stringg
    n = 0
    while n != 4:
        x = emptyline1(string,n)
        string = x[0]
        n = x[1]
    n = 1
    while n != 0:
        x = emptyline2(string,n)
        string = x[0]
        n = x[1]
    return string

def removehcn(string,n):
    numbers = ['0','1','2','3','4','5','6','7','8','9']
    na = string.find('\chapter{',n)
    nb = string.find('\section{',n)
    if na != nb:
        if na == -1:
            n = nb
        elif nb == -1:
            n = na
        else:
            n = min(na,nb)
        if string[n+9] in numbers:
            n2 = string.find(' ',n)
            string = string[0:n+9] + string[n2+1:]
    else:
        n = na
    return [string,n]

def removehcns(string):
    n = 0
    while n != -1:
        n = n + 1
        x = removehcn(string,n)
        string = x[0]
        n = x[1]
    return string

def abstract(string):
    file = open(string)
    string = file.read()
    file.close()
    n = string.find("<div id='abstract'>")
    if n != -1:
        m = string.find('</div>',n)
        string = string[n+19:m]
    return string

def superimp(string):
    string = string.replace(",''","\\rlap{,}''")
    string = string.replace(".''","\\rlap{.}''")
    string = string.replace("'',","\\rlap{,}''")
    string = string.replace("'',","\\rlap{.}''")
    return string

def mainmatter(string):
    n = string.find('\\chapter{')
    s1 = string[0:n+5]
    s2 = string[n+5:]
    s2 = s2.replace('\\chapter{','\\mainmatter\\chapter{',1)
    return s1 + s2

def WordCount(string):
    convcmd = ['pandoc','-f','html','-t','plain']
    string = bytes(string.encode())
    string = subprocess.check_output(convcmd,input=string)
    wc = subprocess.check_output(['wc','-w'],input=string)
    wc = wc.decode()
    #wc = int(wc)//1000
    #wc = 1000*wc
    return wc

def nformat(i,lang):
    if lang == 'fi':
        space = ' '
    else:
        space = ','
    i = str(i)
    return i[0:-4] + ' ' + i[-4:]

def insertmetadata(html,lang):
    html = html.replace('DateGoesHere',Date())
    html = html.replace('DateFormattedGoesHere',dateformat(Date(),lang))
    html = html.replace('CommitIdGoesHere',CommitId())
    html = html.replace('CommitIdShortGoesHere',CommitId()[0:8])
    html = html.replace('WordCountGoesHere',nformat(WordCount(html),lang))
    return html

def squarecommit(html):
    comid = CommitId()
    html = html.replace('scid1',comid[0:8])
    html = html.replace('scid2',comid[8:16])
    html = html.replace('scid3',comid[16:24])
    html = html.replace('scid4',comid[24:32])
    html = html.replace('scid5',comid[32:])
    return html

def htmlebook(string):
    f = open('./preamble.html')
    preamb = f.read()
    f.close()
    n = string.find('<body>') + 6
    #string = string[0:n] + preamb + string[n:]
    string = string.replace('<!--AlkuSysteemi-->',preamb)
    string = citetoem(string)
    #string = htmltoc(string)
    string = squarecommit(string)
    string = insertmetadata(string,'fi')
    return string

def latex(string):
    f = open('./template.tex')
    temp = f.read()
    f.close()
    f = open('./alkusivut.tex')
    alkusivut = f.read()
    f.close()

    TexConvCmd = ['pandoc','-f','html','-t','latex']
    string = citetoem(string)
    string = emptylines(string)
    string = bytes(string.encode())
    string = subprocess.check_output(TexConvCmd,input=string)
    string = string.decode()
    string = latexheaders(string)
    #temp = utf8tolatex(temp)
    string = links(string)
    string = crossrefs(string)
    #temp = temp.replace('\n\\itemsep1pt\\parskip0pt\\parsep0pt\n','\n\\itemsep2pt\\parskip0pt\\parsep0pt\n')
    #temp = temp.replace('\n\nNoBlankLine348\n\n','\n')
    temp = temp.replace('MainContentGoesHere',string)
    temp = temp.replace('AlkusivutGoesHere',alkusivut)
    temp = temp.replace('LanguageGoesHere','finnish')
    string = string.replace("DateGoesHere", Date())
    temp = mainmatter(temp)
    temp = temp.replace('CommitIdGoesHere',CommitId())
    temp = temp.replace('\n\nNoBlankLine348\n\n','\n')
    temp = temp.replace('\n\\itemsep1pt\\parskip0pt\\parsep0pt\n','\n\\itemsep2pt\\parskip0pt\\parsep0pt\n')
    temp = temp.replace('\\includegraphics{','\\noindent\\includegraphics[width=\\textwidth]{')
    temp = temp.replace('LaTeX','{\\LaTeX}')
    return temp

"""
    file = open('./book.yml')
    structure = file.read()
    file.close()
    structure = yaml.load(structure)

    file = open(structure['PDFTemplate'])
    document = file.read()
    file.close()

    TexConvCmd = ['pandoc','-f','html','-t','latex']

    MainContent = concatenation(structure['Chapters'])
    MainContent = emptylines(MainContent)
    #print(MainContent)
    #print(emptyline1(MainContent,80000)[1])
    MainContent = citetoem(MainContent)
    MainContent = bytes(MainContent.encode())
    MainContent = subprocess.check_output(TexConvCmd,input=MainContent)
    MainContent = MainContent.decode()
    MainContent = latexheaders(MainContent)
    MainContent = utf8tolatex(MainContent)
    MainContent = links(MainContent)
    MainContent = MainContent.replace('\n\\itemsep1pt\\parskip0pt\\parsep0pt\n','\n\\itemsep2pt\\parskip0pt\\parsep0pt\n')
    MainContent = MainContent.replace('\n\nNoBlankLine348\n\n','\n')


    document = metadata(structure['Metadata'],document)

    document = document.replace('MainContentGoesHere',MainContent)

    document = removehcns(document)
    document = document.replace('LaTeX','{\\LaTeX}')
    #document = superimp(document)

    print(document)
"""
def xmlmetadata(fp):
    f = open(fp)
    metad = f.read()
    f.close()
    metad = metad.replace('DateGoesHere',Date())
    return metad

def main():
    frontpage = sys.argv[1]
    t = sys.argv[2]
    html = singlepage(frontpage)
    if t == 'html':
        print(htmlebook(html))
    elif t == 'metadata':
        print(xmlmetadata(frontpage))
    else:
        print(latex(html))

if __name__ == '__main__':
    main()
