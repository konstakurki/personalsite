# Tieteellinen prosessi—muistiinpanot

*   Uskontoja ja valheita puolustetaan voimakkaasti
*   Tieteellinen prosessi on meillä verissä. Käytännössä kaikki oppimiseen liittyvät virhekäsitykset ovat tieteellisen prosessin virhetulkintaa.
*   Kokeneita asiantuntijoita arvostetaan, koska ajatellaan, että he ovat uransa aikana ehtineet testata monia teorioita ekstensiivisesti. Tämä on usein epätotta: on aivan mahdollista tehdä pitkä ura samaa kaavaa toistaen.
*   Monimutkaisuutta arvostetaan, sillä usein hyvä, tai ainakin huonoa parempi, ratkaisu on monimutkainen. Tässä on helppo paikka huijaamiselle: monimutkaisuutta on vaikea ymmärtää, joten minkä tahansa sotkun voi saada näyttämään vakuuttavalta.
*   Tässä ei sinänsä ole mitään ihmeellistä: kaikki huijausmenetelmät matkivat jotakin todellista, ja kaikkea todellista matkimaan on syntynyt huijausmenetemiä. Niinpä mistään asiasta ei voi suoraan sanoa, että onko kyse huijaamisesta vai todellisesta. On käytettävä omaa järkeä.
*   Oppimista voi tehostaa omaksumalla tavan testata ideaa välittömästi. Kun olin suihkussa, huomasin että rikkinäisen suihkukahvan telineen voisi varmaankin korjata nippusiteellä. Laitoin suihkun kiinni, menin autotalliin etsimään nippusidettä, palasin sellaisen kanssa, korjasin suihkun.
*   Tietokoneohjelmoinnissa testausnopeutta on helppo lisätä: käytä interaktiivista shelliä/REPLiä. Kirjoita shelliskripti joka kääntää ja ajaa ohjelmasi.
*   Tunteet on pääasiallinen instrumentti teorian toimivuuden tarkastelussa. Jos teoria ei toimi, on fiilis sillon jollain tavalla tahmea. "Jokin on pielessä."
